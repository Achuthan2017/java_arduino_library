package arduino_com;

import java.util.Scanner;



public class test_arduino {
	
	public static void main(String[] args) {
		
		Scanner ob = new Scanner(System.in);
		Arduino arduino = null;
		try {
			arduino = new Arduino("COM3", 9600);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}

		arduino.registerEventReaderCb(new EventReader() {
			public void eventReaderCallback(String message) {
				System.out.println("Read in test_arduino >>  " + message);
			}
		});
		


		System.out.println("Enter 1 to switch LED on and 0  to switch LED off");
		char input = ob.nextLine().charAt(0);
		
		while(input != 'n'){
			if(input=='1') {

				String res = arduino.get_mode();
				System.out.println(res);
				res = arduino.get_version();
				System.out.println(res);

			}
			if(input=='2') {
				String res = arduino.pin_mode(0,"INPUT");
				System.out.println(res);
			}
			
			input = ob.nextLine().charAt(0);
		}
		ob.close();
		arduino.closeConnection();
	
	}

}
