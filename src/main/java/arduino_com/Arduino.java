package arduino_com;

import com.fazecast.jSerialComm.*;


/**
* <h1> Java Arduino Communication </h1>
* This implementation provides handles to control the
* periferials and IO of a connected Arduino.
* 
* This implementation uses the serial Arduino <-> Java communication
* interface provided by "hirdaygupta"
*         https://sourceforge.net/projects/javaarduinolibrary/files/
*
* With inspiration from Tristan A. Hearn is Python Arduino Command API
*         https://github.com/thearn/Python-Arduino-Command-API
*
* @author  Achuthan Paramanathan
* @version 0.1
* @since   2021-12-23
*/
public class Arduino extends SerialCommHandler{

	String libraryVersion = "V0.7";

	public Arduino(String portDescription) {
		this.portDescription = portDescription;
		comPort = SerialPort.getCommPort(this.portDescription);
		this.baud_rate = 9600;
		this.setBaudRate(this.baud_rate);
		//this.startListener();
	}

	public Arduino(String portDescription, int baud_rate) throws Exception {
		//preferred constructor
		this.portDescription = portDescription;
		comPort = SerialPort.getCommPort(this.portDescription);
		this.baud_rate = baud_rate;
		comPort.setBaudRate(this.baud_rate);
		//this.startListener();

		boolean booted = this.bootup_and_valid();
		if (!booted) {
			throw new Exception("Booting of Arduino failed!");
		}
	}

	public String pin_mode(int pin, String dir)
	{
		String ret = "";
		int _pin = 0;

		if("INPUT" == dir)
		{
			_pin = -pin;
		}
		else
		{
			_pin = pin;
		}
		int[] args = {4, 5};
		args[0] = _pin;
		String cmd_str = this.build_command_string("aw", args);
		this.serialWrite(cmd_str);
		cmd_str = this.serialRead(0, 100);

		return cmd_str;
	}

	public String get_mode()
	{
		String cmd_str = this.build_command_string("mode", null);
		this.serialWrite(cmd_str);
		cmd_str = this.serialRead(0, 100);
		return cmd_str;
	}

	public String get_version()
	{
		String cmd_str = this.build_command_string("version", null);
		this.serialWrite(cmd_str);
		cmd_str = this.serialRead(0, 100);
		return cmd_str;
	}

	private boolean bootup_and_valid() {

		System.out.println("Connecting to Arduino...");
		boolean response = false;

		if(this.openConnection())
		{
			System.out.println("\t >>> Connected");
			String version = get_version();
			response = version.equals(this.libraryVersion);

			if(!response) {
				System.out.println("\t You need to update the version of the Arduino-Python3 library running on your Arduino.");
				System.out.println("\t The Arduino sketch is " + version);
				System.out.println("\t The Python installation is " + this.libraryVersion);
				System.out.println("\t Flash the prototype sketch again.");
			}
		}
		else{
			System.out.println("\t >>> Connection failed");
		}

		return response;
	}

	private void start_listener() {

		comPort.addDataListener(new SerialPortDataListener() {

			public int getListeningEvents() {
				return SerialPort.LISTENING_EVENT_DATA_AVAILABLE;
			}

			public void serialEvent(SerialPortEvent event) {

				if (event.getEventType() != SerialPort.LISTENING_EVENT_DATA_AVAILABLE)
				{
					return;
				}

				byte[] newData = new byte[comPort.bytesAvailable()];
				int numRead = comPort.readBytes(newData, newData.length);
				String string = new String(newData);
				if (eventReader != null)
				{
					eventReader.eventReaderCallback(string);

				}
			}
		});
	}

}
