package arduino_com;
import com.fazecast.jSerialComm.*;

import java.io.PrintWriter;
import java.util.Scanner;

public class SerialCommHandler {

    public SerialPort comPort;
    public String portDescription;
    public int baud_rate;

    EventReader eventReader = null;


    public String build_command_string(String cmd, int args[])
    {
        String built_cmd = null;

        if(args!=null)
        {
            built_cmd = "%"+args[0];

            for (int i = 1; i<args.length; i++ )
            {
                built_cmd = built_cmd.concat("%"+args[i]);
            }

            built_cmd = String.format("@%s%s$!", cmd, built_cmd);
        }
        else
        {
            built_cmd ="@"+cmd;
            built_cmd = built_cmd.concat("%0");
            built_cmd = built_cmd.concat("$!");
        }
        System.out.println(built_cmd);
        return built_cmd;
    }

    public boolean openConnection(){
        if(comPort.openPort()){
            try {
                Thread.sleep(100);
            } catch(Exception e){}

            String ret = this.serialRead(0, 1500);
            return (ret.equals("connected"));
        }
        else {
            System.out.println("Error Connecting Try Another port");
            return false;
        }
    }

    public void closeConnection() {
        comPort.closePort();
    }


    public void registerEventReaderCb(final  EventReader callback) {
        eventReader = callback;
    }

    public void setPortDescription(String portDescription){
        this.portDescription = portDescription;
        comPort = SerialPort.getCommPort(this.portDescription);
    }

    public void setBaudRate(int baud_rate){
        this.baud_rate = baud_rate;
        comPort.setBaudRate(this.baud_rate);
    }

    public String getPortDescription(){
        return portDescription;
    }

    public SerialPort getSerialPort(){
        return comPort;
    }

    public String serialRead(int limit, int timeout){
        //will be an infinite loop if incoming data is not bound
        comPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING, timeout, 0);
        String out="";
        Scanner in = new Scanner(comPort.getInputStream());
        try
        {
            while(in.hasNext())
                out += (in.next()+"\r\n");
            in.close();
        } catch (Exception e) { e.printStackTrace(); }
        return out.replaceAll("(\\r|\\n)", "");
    }

    public String serialRead(int limit){
        //in case of unlimited incoming data, set a limit for number of readings
        comPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING, 0, 0);
        String out="";
        int count=0;
        Scanner in = new Scanner(comPort.getInputStream());
        try
        {
            while(in.hasNext()&&count<=limit){
                out += (in.next()+"\n");
                count++;
            }
            in.close();
        } catch (Exception e) { e.printStackTrace(); }
        return out;
    }

    public void serialWrite(String s){
        //writes the entire string at once.
        comPort.setComPortTimeouts(SerialPort.TIMEOUT_SCANNER, 0, 0);
        try{Thread.sleep(5);} catch(Exception e){}
        PrintWriter pout = new PrintWriter(comPort.getOutputStream());
        pout.print(s);
        pout.flush();

    }

    public void serialWrite(String s,int noOfChars, int delay){
        //writes the entire string, 'noOfChars' characters at a time, with a delay of 'delay' between each send.
        comPort.setComPortTimeouts(SerialPort.TIMEOUT_SCANNER, 0, 0);
        try{Thread.sleep(5);} catch(Exception e){}
        int i = 0;
        PrintWriter pout = new PrintWriter(comPort.getOutputStream());
        for(i = 0; i < s.length() - noOfChars; i += noOfChars){
            pout.write(s.substring(i,i+noOfChars));
            pout.flush();
            try{Thread.sleep(delay);}catch(Exception e){}
        }
        pout.write(s.substring(i));
        pout.flush();

    }

    public void serialWrite(char c){
        //writes the character to output stream.
        comPort.setComPortTimeouts(SerialPort.TIMEOUT_SCANNER, 0, 0);
        try{Thread.sleep(5);} catch(Exception e){}
        PrintWriter pout = new PrintWriter(comPort.getOutputStream());pout.write(c);
        pout.flush();
    }

    public void serialWrite(char c, int delay){
        //writes the character followed by a delay of 'delay' milliseconds.
        comPort.setComPortTimeouts(SerialPort.TIMEOUT_SCANNER, 0, 0);
        try{Thread.sleep(5);} catch(Exception e){}
        PrintWriter pout = new PrintWriter(comPort.getOutputStream());pout.write(c);
        pout.flush();
        try{Thread.sleep(delay);}catch(Exception e){}
    }
}
